//
//  TouchDelegatorViw2.h
//  TestGesture
//
//  Created by Nicolas NOUIRA on 15/09/2016.
//  Copyright © 2016 BEEPEERS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TouchDelegatorView2 : UIView


@property (nonatomic) UIView * view;

@end
