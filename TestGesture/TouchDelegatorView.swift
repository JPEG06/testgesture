//
//  TouchDelegatorView.swift
//  TestGesture
//
//  Created by Nicolas NOUIRA on 15/09/2016.
//  Copyright © 2016 BEEPEERS. All rights reserved.
//

import UIKit

class TouchDelegatorView: UIView, UIGestureRecognizerDelegate ,UIScrollViewDelegate{
    
    var view:UIView!
    var gesture:UIGestureRecognizer
    var currentGesture:UIGestureRecognizer
    
    
    required init?(coder aDecoder: NSCoder) {
        self.gesture = UIGestureRecognizer.init();
        self.currentGesture = UIGestureRecognizer.init();
        super.init(coder: aDecoder);
    }
    
    func putView (viewDelegated:UIView) {
       
        self.view = viewDelegated;
        
        
        //self.userInteractionEnabled = false;
//        
////        if self.view.isKindOfClass(UIScrollView) {
////            (self.view as! UIScrollView).delegate = self;
////        }
//        for toto in view.gestureRecognizers! {
//            self.bounds = self.view.bounds;
////            if toto.isKindOfClass(UIPanGestureRecognizer) {
//                let gestTmp = UIPanGestureRecognizer.init();
//                let targets = (toto.valueForKeyPath("_targets") as! NSArray);
//                print("touchesBegan");
//                self.addGestureRecognizer(gestTmp);
//                gestTmp.setValue(targets, forKeyPath: "_targets")
//                gestTmp.delegate = toto.delegate;
////            }
//            
//        }
//        print("touchesBegan");
////        self.view.addGestureRecognizer(pangesture);
////        
////        let pangesture2 = UIPanGestureRecognizer.init();
////        //        pangesture.addTarget(self, action: #selector(toto));
////        pangesture2.delegate = self;
        
    }
    
    
    
    //We tested with touch here
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.touchesBegan(touches, withEvent: event)
    }
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.touchesEnded(touches, withEvent: event)
    }
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.touchesMoved(touches, withEvent: event)
    }
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        self.view.touchesCancelled(touches, withEvent: event)
    }
    
    
//    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
//        ret
//    }
//    
//    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceivePress press: UIPress) -> Bool {
//        
//    }
//    
//    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOfGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
//        
//    }
//    
//    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailByGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
//        
//    }
    
//    override func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
//        return true;
//    }
//    
//    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
//        return true;
//    }
    
    

}
